import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Auth } from '../auth/entities/auth.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Auth) private readonly userRepository: Repository<Auth>,
  ) {}
  findAll(): Promise<Auth[]> {
    return this.userRepository.find();
  }

  findOne(id: string): Promise<Auth> {
    return this.userRepository.findOne({ where: { id } });
  }
}
