import { Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { Repository } from 'typeorm';
import { Auth } from '../auth/entities/auth.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post) private readonly postRepository: Repository<Post>,
    @InjectRepository(Auth) private readonly userRepository: Repository<Auth>,
  ) {}

  async createPost(
    createPostDto: CreatePostDto,
    userId: string,
  ): Promise<Post> {
    const user: Auth = await this.userRepository.findOne({
      where: { id: userId },
    });

    if (!user) {
      throw new Error('User not found');
    }

    const post: Post = new Post();
    post.content = createPostDto.content;
    post.user = user;

    try {
      return await this.postRepository.save(post);
    } catch (error) {
      throw new Error(`Failed to create post: ${error.message}`);
    }
  }

  async findPostsByUserId(userId: string): Promise<Post[]> {
    return this.postRepository.find({ where: { user: { id: userId } } });
  }

  async findOnePost(id: string): Promise<Post> {
    return this.postRepository.findOne({ where: { id } });
  }

  async updatePost(
    id: string,
    updatePostDto: UpdatePostDto,
  ): Promise<Post | undefined> {
    const post = await this.findOnePost(id);

    if (!post) {
      return undefined;
    }
    post.content = updatePostDto.content;
    return this.postRepository.save(post);
  }

  async removePost(id: string): Promise<void> {
    await this.postRepository.delete(id);
  }
}
