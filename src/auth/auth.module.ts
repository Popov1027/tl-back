import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { LoginService } from './login.service';
import { Auth } from './entities/auth.entity';
import { Login } from './entities/login.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Auth, Login]),
    JwtModule.register({
      secret: 'postgres',
      signOptions: { expiresIn: 3600 },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, LoginService],
})
export class AuthModule {}
