import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import { LoginService } from './login.service';
import { LoginDto } from './dto/login.dto';
import { ApiTags } from '@nestjs/swagger';
import { Auth } from './entities/auth.entity';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly loginService: LoginService,
  ) {}

  @Post()
  createAuth(@Body() authDto: AuthDto): Promise<Auth> {
    return this.authService.auth(authDto);
  }

  @Post('/login')
  createLogin(@Body() loginDto: LoginDto): Promise<{ token: string }> {
    return this.loginService.login(loginDto);
  }
}
